# RNA-Seq pipeline

## Converting Illumina run to FASTQ
* [bcl2fastq](https://support.illumina.com/content/dam/illumina-support/documents/documentation/software_documentation/bcl2fastq/bcl2fastq2_guide_15051736_v2.pdf)

```
bcl2fastq 
	--runfolder-dir <path-to-nextseq-run>
	--output-dir <path-to-save-fastq-samples>
	--no-lane-splitting
	--loading-threads 8
	--demultiplexing-threads 8
	--writing-threads 8
	--sample-sheet /path/to/SampleSheet.csv
```

## Aligning reads to genome

* [STAR](http://chagall.med.cornell.edu/RNASEQcourse/STARmanual.pdf)

```
START
	--runMode alignReads
	--runThreadN 8
	--genoeDir <path-to-genome-star-index>
	--readFilesIn <path-to-fastq-file>
	--readFilesCommand zcat
	--outStd Log
	--outSAMtype BAM SortedByCoordinate
	--outSAMstrandField intronMotif
	--outFilterIntronMotifs RemoveNoncanonical
	--alignSoftClipAtReferenceEnds No
	--outFilterScoreMinOverLread 0.25
	--outFilterMatchNminOverLread 0.25
```

Note: sometimes depend on your seq. library you will need to update the parameters to optimise the alignment. More parameters can be found in the manual.

## Annotate alignments
* [FeatureCounts](http://bioinf.wehi.edu.au/featureCounts/)

```
featureCounts
	-a <path-to-GTF-genome-annotation>
	-o <path-to-save-counts>/counts.txt
	-t exon
	-Q 255
	-T 8
	$(ls <path-to-BAM-file>/*.bam)
```

## Differential expressions with R
* [DESeq2](https://bioconductor.org/packages/release/bioc/html/DESeq2.html)
* [EdgeR](https://bioconductor.org/packages/release/bioc/html/edgeR.html)

## Other useful tools

* [samtools](http://samtools.sourceforge.net/)
* [bedtools](https://bedtools.readthedocs.io/en/latest/)
* [tabix](http://www.htslib.org/doc/tabix.html)

